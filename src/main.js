import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import axios from 'axios'

/*Vue.prototype.$api = axios.create({
  baseURL: 'https://tools.sopress.net/iut/panier/api/',
  params:{
    token: 'yann.hittin8@etu.univ-lorraine.fr'
  }
});*/

Vue.config.productionTip = false;
Vue.prototype.$bus = new Vue();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
