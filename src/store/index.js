import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

export default new Vuex.Store({
	plugins: [vuexLocal.plugin],
	state: {
		amogus : {
			position : '',
			trouve : false
		},
		lumieres: {
			salon : false,
			chambre : false
		}
	},
	mutations: {
		setLumiere(state, piece) {
			state.lumieres[piece] = !state.lumieres[piece];
		},
		setOuEstAmogus(state, piece) {
			state.amogus.position = piece;
			if(!state.amogus.trouve) {
				state.amogus.trouve = true;
			}
		}
	},
	actions: {
	},
	modules: {
	}
})
