import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/salon',
    name: 'Salon',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Salon.vue')
  },
  {
    path: '/grenier',
    name: 'Grenier',
    component: () => import('../views/Grenier.vue')
  },
  {
    path: '/cuisine',
    name: 'Cuisine',
    component: () => import('../views/Cuisine.vue')
  },
  {
    path: '/chambre',
    name: 'Chambre',
    component: () => import('../views/Chambre.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
